#!/bin/sh
export JIRA_HOME="$HOME/src/jira"

test_copy() {
    ls -a  "$1"
}
# USAGE: copy-untracked.sh FROM TO
copy_untracked() {
    git -C "$JIRA_HOME/$1" ls-files -oz --directory | xargs -0 -I'{}' -n1 cp -r "$JIRA_HOME/$1/{}" "$JIRA_HOME/$2/{}"
}

# new worktree creating a new branch
gwtree_nb() {
    git -C $HOME/src/jira/master worktree add -b "issue/$1" "$HOME/src/jira/$1"
}

# new worktree from existing  branch
gwtree_eb() {
    git -C $HOME/src/jira/master worktree add "$HOME/src/jira/$1" "issue/$1"
}

# removes a worktree
gwtree_rm() {
    git -C $HOME/src/jira/master worktree remove "$HOME/src/jira/$1"
}

