#!/bin/bash
set -e # exit immediately if a command exists with a non-zero status

CURRENT_DIR=$(pwd)

# installing brew
if [ ! -d /usr/local/Homebrew ]; then
  echo "*** installing brew ***"
  BREWINSTALLER=$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install) &&
    ruby -e "$BREWINSTALLER" || echo "Failed to install brew :("

  brew update
fi

# installing sdkman
if [ ! -d $HOME/.sdkman ]; then
  echo "*** installing sdkman ***"
  curl -s "https://get.sdkman.io" | bash
  echo "run: source $HOME/.sdkman/bin/sdkman-init.sh"
fi

# creates bash_profile as a link to dotfiles
if [ ! -f $HOME/.bash_profile ]; then
  ln -sf $CURRENT_DIR/bash_profile $HOME/.bash_profile
fi

# creates add ons links to files on add ons dir
for n in `ls $CURRENT_DIR/addons`; do
  cp $CURRENT_DIR/addons/$n $HOME/.$n
done

echo "run the following command:"
echo "source $HOME/.bash_profile"

