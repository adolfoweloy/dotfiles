#!/bin/bash
#export NVM_DIR=~/.nvm && source /usr/local/opt/nvm/nvm.sh
#export JAVA_HOME=$(/usr/libexec/java_home -v 1.8)

DOTFILES_HOME=$HOME/src/dotfiles # this should be dynamic
PLUGINS=$DOTFILES_HOME/plugins

# loads all additional modular settings
for n in `ls $PLUGINS`; do
    source $PLUGINS/$n
done

# set vi as the in-line editor
set -o vi

#THIS MUST BE AT THE END OF THE FILE FOR SDKMAN TO WORK!!!
export SDKMAN_DIR="/Users/aeloy/.sdkman"
[[ -s "/Users/aeloy/.sdkman/bin/sdkman-init.sh" ]] && source "/Users/aeloy/.sdkman/bin/sdkman-init.sh"
